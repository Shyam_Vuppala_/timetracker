'use strict';

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  Button,
  Alert,
  AsyncStorage,
  TouchableOpacity,
} from 'react-native';

import Spinner from 'react-native-loading-spinner-overlay';

import APIConstants from './APIConstants'


export default class LoginPage extends Component {

  static navigationOptions = {
    title: 'Time Tracker',
    headerTintColor: '#ffffff',
    headerStyle: {
      backgroundColor: '#2F95D6',
      borderBottomColor: '#ffffff',
      borderBottomWidth: 3,
    },
    headerTitleStyle: {
      fontSize: 18,
    },
  };
  constructor(props) {
    super(props);
    this.state = {
      userName:'',
      password:'',
      visible: false
    }
  }

  _onSearchTextChanged = (event) => {
  };

  _onLoginPressed = () => {
    const error = this.validateLogin()
    if (error == '') {
      this.login()
    } else {
      Alert.alert(
        error
      )
    }
  };

  validateLogin() {
    let userName = this.state.userName
    let password = this.state.password
    if (userName == '') {
      return 'Please enter email'
    }
    else if(password == '') {
      return 'Please enter password'
    }
    return ''
  }

  login = () => {
    let userName = this.state.userName
    let password = this.state.password
    
    this.setState({
        visible: true
    });
    return fetch(APIConstants.BASE_URL + APIConstants.LOGIN, {
      method: 'POST',
      headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      login_id : userName
    }),
    }).then((response) => response.json())
      .then((responseJson) => {
        console.log('Response JSON ' + responseJson)
          this.setState({
              visible: false
          });
        if (responseJson.status == 'OK' && responseJson.message == 'Success') {
          
          AsyncStorage.setItem('user_id', JSON.stringify(responseJson));
          const { navigate } = this.props.navigation;
          navigate('Results', { name: 'ContactList'})
        } else {
          Alert.alert(
            'Please enter valid credentials'
         )
        }
      })
      .catch((error) =>{
        console.error(error);
      });
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.flowRight}>
      
          <TextInput
            style={styles.searchInput}
            onSubmitEditing={() => this.passwordInput.focus()}
            autoCapitalize="none"
            keyboardType='email-address' 
            returnKeyType="next"  
            onChangeText={(userName) => this.setState({userName})}
            onChange={this._onSearchTextChanged}
            underlineColorAndroid='transparent'
            placeholder='Email'/>

          <TextInput
            style={styles.searchInput}
            ref={(input)=> this.passwordInput = input} 
            onChangeText={(password) => this.setState({password})}
            onChange={this._onSearchTextChanged}
            secureTextEntry={true}
            underlineColorAndroid='transparent'
            placeholder='Password'/>
          
          <TouchableOpacity
            style={styles.LoginButtonStyle}
            activeOpacity = { .5 }
            onPress={ this._onLoginPressed } >
            <Text style={styles.TextStyle}> LOGIN </Text>
          </TouchableOpacity>

          <Spinner visible={this.state.visible} textContent={""} textStyle={{color: '#FFF'}} />

        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  description: {
    marginBottom: 20,
    fontSize: 18,
    textAlign: 'center',
    color: '#656565'
  },
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  flowRight: {
    flexDirection: 'column',
    alignItems: 'center',
    alignSelf: 'stretch',
    justifyContent: 'center',
  },
  searchInput: {
    width: 200, 
    height: 36,
    padding: 4,
    marginRight: 5,
    marginBottom:10,
    fontSize: 18,
    borderWidth: 1,
    borderColor: '#48BBEC',
    borderRadius: 8,
    color: '#48BBEC',
  },
  button: {
    marginRight:30,
    marginLeft:30,
    marginTop:10,
    paddingTop:20,
    paddingBottom:20,
    backgroundColor:'#F5FCFF',
    borderRadius:10,
    borderWidth: 1,
    borderColor: '#00BCD4',
    color:'white',
  },
    LoginButtonStyle: {

        marginTop:10,
        paddingTop:10,
        paddingBottom:24,
        marginLeft:30,
        marginRight:30,
        backgroundColor:'#2F95D6',
        borderRadius:20,
        borderWidth: 1,
        borderColor: '#2F95D6',
        width: 160,
        height: 40,
    },
    MainContainer: {
        //flex: 1,
        flexDirection: 'column',
        backgroundColor: '#F5FCFF',
        alignItems: 'center',
        height: 30,
    },

    TextStyle:{
        color:'#fff',
        textAlign:'center',
        height: 30,
    },

});
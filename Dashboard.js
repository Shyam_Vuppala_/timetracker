'use strict';

import React, { Component, PropTypes } from 'react';
import CalendarStrip from 'react-native-calendar-strip';
import moment from 'moment';
import Task from './Task'
import Spinner from 'react-native-loading-spinner-overlay';

import {
  StyleSheet,
  Image,
  View,
  TouchableHighlight,
  FlatList,
  Text,
  Platform,
  TouchableOpacity,
  Animated,
  ScrollView,
  Button,
  Dimensions,
  ActivityIndicator,
  AsyncStorage,

} from 'react-native';

import APIConstants from './APIConstants';

export default class TaskList extends Component {

  static navigationOptions = {
    title: 'Dashboard',
    headerTintColor: '#ffffff',
    headerStyle: {
      backgroundColor: '#2F95D6',
      borderBottomColor: '#ffffff',
      borderBottomWidth: 3,
    },
    headerTitleStyle: {
      fontSize: 18,
    },
  };

  _keyExtractor = (item, index) => index.toString();

  _renderItem = ({item, index}) => (
    <Task key={index}
      onPress={() => {const selectedTask = this.state.dataSource[index]
                      console.log('selected Task ' + selectedTask)
                      const { navigate } = this.props.navigation;
                      navigate('AddTask', { name: 'AddTask', selectedDate:this.state.selectedDate, task:selectedTask } )
      }}
      task={item} onRemoveListItem={this.removeListItem.bind(item.log_id,index)}/>
  );

  removeListItem = (id,index) => {
    console.log('index --- ' + id + index);
    console.log(this.state.dataSource[id])
      let log_id = this.state.dataSource[id].log_id
      this.deleteTask(log_id)
    // this.setState({
    //   dataSource: this.state.dataSource.filter(item => item.log_id === id)
    // });


    // let newItems = this.state.dataSource.splice(index, 2)
    // // This will remove the element at index, and update this.items with new array
    // console.log(newItems)
    // this.setState({
    //   dataSource: newItems
    // });
  }

  _onPress = (index) => {
    console.log('onPress')
    const selectedTask = this.state.dataSource[index]
    console.log('selected Task ' + selectedTask)
    // const { navigate } = this.props.navigation;
    // navigate('AddTask', { name: 'AddTask', isNewTask:true, selectedDate:this.state.selectedDate, task:selectedTask } )
  }

  addNewItem = () => {
    console.log('Add new item')
    const { navigate } = this.props.navigation;
    navigate('AddTask', { name: 'AddTask', selectedDate:this.state.selectedDate, task:null})
  }

  constructor(props) {
      super(props);
      this.state = {
        dataSource: [],
        user_id: -1,
        selectedDate:moment(),
        visible: false
      }
      //this.onDateSelected = this.onDateSelected.bind(this);
  }

 componentDidMount(){
   console.log('componentDidMount');
   let date = moment().format('YYYY-MM-DD')

   AsyncStorage.getItem('user_id', (err, result) => {
    let json = JSON.parse(result)
    console.log('user_id ' + json.id )
    this.setState({
      user_id: json.id
    })
    this.getTaskList(date)
   });
 }

 getTaskList = (date) => {
    console.log('user id ' + this.state.user_id)
    console.log('date ' + date)
     this.setState({
         visible: true
     });
   return fetch(APIConstants.BASE_URL + APIConstants.GET_LIST_OF_TASKS, {
     method: 'POST',
     headers: {
     Accept: 'application/json',
     'Content-Type': 'application/json',
   },
   body: JSON.stringify({
     'emailId': 'harikrishna.gavidi@gmail.com',
     'accessToken': 'accessToken',
     'userId': this.state.user_id,
     'date': date
   }),
   }).then((response) => response.json())
     .then((responseJson) => {
       console.log(responseJson)
       this.setState({
           visible: false,
           dataSource: responseJson.tasks,
       }, function(){

       });

     })
     .catch((error) =>{
       console.error(error);
     });
 }

    deleteTask = (log_id) => {

        this.setState({
            visible: true
        });
        return fetch(APIConstants.BASE_URL + APIConstants.DELETE_TASK, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                'logID': log_id
            }),
        }).then((response) => response.json())
            .then((responseJson) => {
                console.log(responseJson)

                this.getTaskList(this.state.selectedDate.format('YYYY-MM-DD'))
                this.setState({
                    visible: false,
            }, function(){

                });


            })
            .catch((error) =>{
                console.error(error);
            });
    }

 onDateSelected = (selectedDate) => {
    let date = selectedDate.format('YYYY-MM-DD')
    console.log('Selected Date ' + date)
    this.setState({
      selectedDate: selectedDate,
    })
    this.getTaskList(date)
 }
 isDateSelected = (isDateSelected) => {
  console.log('isDateSelected ' + isDateSelected.format('YYYY-MM-DD'))
}
    render() {
    const {items} = this.props
    //const { params } = this.props.navigation.state;
        let deviceWidth = Dimensions.get('window').width
        let datesBlackList = [{
          start: moment().add(1, 'days'),
          end: moment().add(365, 'days') // total 365 days disabled
        }];
    let today = moment()
    let firstDayOfWeek = moment().firstDayOfWeek
    
    console.log(firstDayOfWeek)
    //if (today.day)
    let startingDate = moment().add(-3, 'days')
    return (
    <View style = { styles.MainContainer }>
        <Spinner visible={this.state.visible} textContent={""} textStyle={{color: '#FFF'}} />

        <ScrollView style={{marginTop: 10, position: 'absolute'}} horizontal={true} ref='_scrollView'>
            <CalendarStrip ref='calendar'
              datesBlacklist={datesBlackList}
              style={{width: deviceWidth, height:100, paddingTop: 10, paddingBottom: 10}}
              //disabledDateNumberStyle={{color: 'red'}}
              highlightDateNameStyle={{color: '#ffffff'}}
              highlightDateNumberStyle={{color: '#ffffff'}}
              //dateNumberStyle={{color: '#2F95D6'}}
              weekendDateNameStyle={{color: 'black'}}
              weekendDateNumberStyle={{color: 'black'}}
              onDateSelected={this.onDateSelected.bind(this)}
              isDateSelected={this.isDateSelected.bind(this)}
              startingDate={this.state.selectedDate}
              daySelectionAnimation={{type: 'background', duration: 300, highlightColor: '#2F95D6'}}
              calendarHeaderStyle={{color: 'black'}}
              calendarColor={'transparent'}

            />
      </ScrollView>
      

      <View style={{flex: 1, marginTop:70, paddingTop:10}}>
      <View style={(this.state.dataSource && this.state.dataSource.length > 0) ? styles.hideNoTasksText : styles.showNoTasksText}>
      <Text style={{fontSize: 20, color: 'black'}}>No tasks found</Text>
      </View>  
        <FlatList style={{paddingTop:20}}
          data={this.state.dataSource}
          renderItem={this._renderItem}
          keyExtractor={this._keyExtractor}
        />
      </View>

      <TouchableOpacity
        activeOpacity = { 0.7 }
        style = { styles.TouchableOpacityStyle }
        onPress = { this.addNewItem } >

        <Image
          source={{uri : 'https://reactnativecode.com/wp-content/uploads/2017/11/Floating_Button.png'}}
          style = { styles.FloatingButtonStyle }
        />
      </TouchableOpacity>

    </View>
    );
  }
}

const deviceWidth = Dimensions.get('window').width
const styles = StyleSheet.create({
  thumb: {
    width: 82,
    height: 82,
    //marginRight: 10
  },
  duration: {
    width: 80,
    height: 80,
    marginRight: 10,
    justifyContent:'center'
  },
  textContainer: {
    flex: 1,
    justifyContent:'center'
  },
  separator: {
    height: 1,
    backgroundColor: '#dddddd'
  },
  task: {
    fontSize: 22,
    fontWeight: 'bold',
    color: '#48BBEC'
  },
  subTask: {
    fontSize: 22,
    color: '#656565'
  },
  comment: {
    fontSize: 20,
    color: '#656565'
  },

  rowContainer: {
    flexDirection: 'row',
    padding:10,
    marginLeft: 10,
    marginRight: 10,
    marginTop: 5,
    marginBottom: 5,
    borderWidth: 1.0,
    borderRadius: 10,
    borderColor: '#d6d7da',
  },
  MainContainer:
    {
        flex: 1,
        backgroundColor: '#eee',
        flexDirection:'column',
        paddingTop: (Platform.OS == 'ios') ? 20 : 0,
        width: deviceWidth,
        marginRight: 10
    },

    Animated_View_Style:
    {
        height: 60,
        backgroundColor: '#FF9800',
        alignItems: 'center',
        justifyContent: 'center',
        margin: 5
    },

    View_Inside_Text:
    {
        color: '#fff',
        fontSize: 24
    },

    TouchableOpacityStyle:{

      position: 'absolute',
      width: 50,
      height: 50,
      alignItems: 'center',
      justifyContent: 'center',
      right: 30,
      bottom: 30,
    },

    FloatingButtonStyle: {

      resizeMode: 'contain',
      width: 50,
      height: 50,
    },

    container: {
    borderRadius: 4,
    borderWidth: 0.8,
    borderColor: '#d6d7da',
    height: 50,
    marginLeft: 20,
    marginRight: 10,
    flex:1,
    flexDirection:'row',
    alignItems:'center'
  },

  showNoTasksText: {
    height: 60,
    width: deviceWidth,
    paddingTop:10,
    alignItems:'center',
    justifyContent: 'center'
  },

  hideNoTasksText: {
    height: 0,
    paddingTop:0,
  },

});

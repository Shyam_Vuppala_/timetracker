'use strict';

import React, { Component } from 'react';
import { AppRegistry, SectionList, StyleSheet, Text, View, Button, Alert } from 'react-native';
import { CheckBox } from 'react-native-elements';

import '@expo/vector-icons';

export default class Settings extends Component {
  static navigationOptions = {
    title: 'Settings',
    headerTintColor: '#ffffff',
    headerStyle: {
      backgroundColor: '#2F95D6',
      borderBottomColor: '#ffffff',
      borderBottomWidth: 3,
    },
    headerTitleStyle: {
      fontSize: 18,
    },
  };
   constructor(props) {
        super(props);
        this.state = {
            checked: false,
            selectedItem: "Manual"
        };
    }
  componentDidMount() {
    //console.log('parameter ' + this.props.navigation.state.params.abd)
  }

    onItemSelected =(item)=>{

      Alert.alert("You are selected " + item)
      this.setState({
          selectedItem: item
      });
  }

  _onLoginPressed = () => {
    const { navigate } = this.props.navigation;
    navigate('Dashboard', { name: 'Dashboard' })
  };

  render() {

      var mode = ['Manual','Semi Automatic','Automatic'] ;
      var ProjectRelatedSettings = ['Project Name', 'Notifications', 'Alarm', 'Reminders'] ;

      return (
      <View style={styles.container}>
        <SectionList
          sections={[
            {title: 'Mode', data: mode},
            {title: 'Project Related Settings', data: ProjectRelatedSettings},
          ]}
          renderItem={({item}) => <View style={styles.completed}>
              <Text style={styles.item}>{item}</Text>
              <CheckBox
                  checked={this.state.selectedItem == item}
                  //onPress={() => this.setState({ checked: !this.state.checked })}
                  onPress={this.onItemSelected.bind(this, item)}
                  hide
              />
          </View>}
          renderSectionHeader={({section}) => <Text style={styles.sectionHeader}>{section.title}</Text>}
          keyExtractor={(item, index) => index}
        />
        <Button
            style={styles.button}
            onPress={this._onLoginPressed}
            color='#000000'
            title='Dashboard'
          />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
   flex: 1,
   paddingTop: 1,
   backgroundColor: 'white',
  },
  sectionHeader: {
    paddingTop: 2,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 2,
    fontSize: 18,
    fontWeight: 'bold',
    backgroundColor: 'rgba(247,247,247,1.0)',
    height: 35,
  },
  item: {
    padding: 10,
    fontSize: 16,
    height: 44,
  },
  button: {
    marginRight:40,
    marginLeft:40,
    marginTop:10,
    paddingTop:20,
    paddingBottom:20,
    backgroundColor:'#68a0cf',
    borderRadius:10,
    borderWidth: 1,
    borderColor: '#fff',
  },
    completed: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
})

// skip this line if using Create React Native App
AppRegistry.registerComponent('AwesomeProject', () => SectionListBasics);

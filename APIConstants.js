export default {
    //BASE_URL: 'http:14.140.150.198:3000',
    //BASE_URL: 'http:10.0.3.2:3000',
    BASE_URL: 'http://localhost:3000',

    LOGIN: '/api/timetracker/getUserDetails',
    GET_LIST_OF_TASKS: '/api/timetracker/getListOfTasks',
    GET_CLIENTS: '/api/timetracker/getclients',
    GET_PROJECTS: '/api/timetracker/getProjects',
    GET_TASKS: '/api/timetracker/getTasks',
    GET_SUB_TASKS: '/api/timetracker/getSubTasks',
    UPDATE_TASK: '/api/timetracker/updateTask',
    CREATE_TASK: '/api/timetracker/createTask',
    DELETE_TASK: '/api/timetracker/deleteTask',
};
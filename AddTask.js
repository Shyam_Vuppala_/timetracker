'use strict';

import React, { Component } from 'react';

import { AppRegistry, SectionList, StyleSheet, Text, Button,TextInput ,View ,PanResponder, AsyncStorage, TouchableOpacity,Alert} from 'react-native';

import { NavigationActions } from 'react-navigation'
import { Picker } from 'react-native-picker-dropdown'
import { Dropdown } from 'react-native-material-dropdown';
import { TextField } from 'react-native-material-textfield';
import DatePicker from 'react-native-datepicker';
import APIConstants from './APIConstants';

export default class AddTask extends Component {
  static navigationOptions = {
    title: 'Add Task',
    headerTintColor: '#ffffff',
    headerStyle: {
      backgroundColor: '#2F95D6',
      borderBottomColor: '#ffffff',
      borderBottomWidth: 3,
    },
    headerTitleStyle: {
      fontSize: 18,
    },
  };
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      drop_down_client : [],
      drop_down_projects : [],
      drop_down_tasks : [],
      drop_down_subtasks : [],
      selectedClientId : "0",
      selectedProjectId : "0",
      selectedTaskId : "0",
      selectedSubtaskId : "0",
      startTime:'00:00',
      endTime:'00:00',
      duration:'00:00',
      comment: '',
      text: '',
      user_id: -1,

  };

  }

  getDuration=(startTime , endTime)=>{

    let startTimeInMinutes = parseInt(startTime.slice(0,2)) * 60 + parseInt(startTime.slice(3,5))
    let endTimeInMinutes = parseInt(endTime.slice(0,2)) * 60 + parseInt(endTime.slice(3,5))
 
    let durationInMinutes = endTimeInMinutes - startTimeInMinutes
    let durationInHours = parseInt(durationInMinutes / 60)
    let durationInMins = durationInMinutes - durationInHours * 60
    let totalDuration = (durationInHours.length > 1) ? durationInHours : ('0' + durationInHours) + ':' + durationInMins
 
    console.log('Total Duration' + totalDuration)
    return totalDuration;
  }

  getEndTime=(startTime ,duration)=>{

    let hrs = ((parseInt(startTime.slice(0,2))+(parseInt(duration.slice(0,2)))*60)+(parseInt(startTime.slice(3,5))+parseInt(duration.slice(3,5))))/60
    let min = ((parseInt(startTime.slice(0,2))+(parseInt(duration.slice(0,2)))*60)+(parseInt(startTime.slice(3,5))+parseInt(duration.slice(3,5))))%60
    return ((hrs.length > 1) ? hrs : ('0' + hrs) + ':' + min);

  }

  updateTask=(task)=>{

    return fetch(APIConstants.BASE_URL + APIConstants.UPDATE_TASK, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
      'Content-Type': 'application/json',
      'accessToken' : 'abc',
    },
    body: JSON.stringify({
      'logID' : task.log_id,
      'clientID' : this.state.selectedClientId,
      'projectID' : this.state.selectedProjectId,
      'taskID' : this.state.selectedTaskId,
      'subtaskID' : this.state.selectedSubtaskId,
      'startTime' : this.state.startTime,
      'duration' : this.getDuration(this.state.startTime ,this.state.endTime),
      'comment' : this.state.comment
    }),
    }).then((response) => response.json())
      .then((responseJson) => {
        console.log(responseJson)
        this.saveDefaultProjectSettings()
        this.props.navigation.dispatch(NavigationActions.back())
        
      })
      .catch((error) =>{
        console.error(error);
      });
  }

  saveDefaultProjectSettings=()=>{

    var defaultProjectSettings = {
        "clientID": this.state.selectedClientId,
        'projectID': this.state.selectedProjectId,
        'taskID': this.state.selectedTaskId,
        'subtaskID': this.state.selectedSubtaskId,
        'drop_down_client': this.state.drop_down_client,
        'drop_down_projects': this.state.drop_down_projects,
        'drop_down_tasks': this.state.drop_down_tasks,
        'drop_down_subtasks': this.state.drop_down_subtasks
    }
    AsyncStorage.setItem('defaultProjectSettings', JSON.stringify(defaultProjectSettings));
}

  insertTask=()=>{
    let date = this.props.navigation.state.params.selectedDate.format('YYYY-MM-DD')
    return fetch(APIConstants.BASE_URL + APIConstants.CREATE_TASK, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
      'Content-Type': 'application/json',
      'accessToken' : 'abc',
    },
    body: JSON.stringify({
      'userID' : this.state.user_id,
      'clientID' : this.state.selectedClientId,
      'projectID' : this.state.selectedProjectId,
      'taskID' : this.state.selectedTaskId,
      'subtaskID' : this.state.selectedSubtaskId,
      'startTime' : this.state.startTime,
      'date' : date,
      'duration' : this.getDuration(this.state.startTime ,this.state.endTime),
      'comment' : this.state.comment
    }),
    }).then((response) => response.json())
      .then((responseJson) => {
        console.log(responseJson)
        
        this.props.navigation.dispatch(NavigationActions.back())
        
      })
      .catch((error) =>{
        console.error(error);
      });

  }
  validForm=()=>{
    if(this.state.selectedClientId=='0'||this.state.selectedProjectId=='0'||
    this.state.selectedTaskId=='0'||this.state.selectedSubtaskId=='0'){
      return false;
    }
    else {
      return true;
    }
  }

  onPressSubmit=() => {

    const task = this.props.navigation.state.params.task
if(this.validForm()){
  if(this.state.startTime == this.state.endTime){
    Alert.alert(
      'Timetracker',
      'Start and End Time Can Not Be Same',
      [
        {text: 'OK', onPress: () => console.log('OK Pressed On Time Error')},
      ],
      { cancelable: false }
    )
  }else{
    if(task==null){this.insertTask()}
    else {this.updateTask(task)}
  }
  }
else{
Alert.alert(
    'Timetracker',
    'Please fill the form',
    [
      {text: 'OK', onPress: () => console.log('OK Pressed On Form Error')},
    ],
    { cancelable: false }
  )
}

    
}


  componentDidMount(){

    AsyncStorage.getItem('user_id', (err, result) => {
      let json = JSON.parse(result)
      this.setState({
        user_id: json.id
      },function () { this.getAllClients(this.state.user_id)});
    });

    if (this.props.navigation.state.params.task == null) {
      AsyncStorage.getItem('defaultProjectSettings', (err, result) => {
        if (result != null) {
          this.setDefaultProjectSettings()
        }
      })
      console.log('Task is null')
    } else {
      this.prepopulateTaskDetails()
      console.log('task ' + this.props.navigation.state.params.task.client_id)
    }
    console.log('selectedDate ' + this.props.navigation.state.params.selectedDate)
    
  }

  getAllClients = (paramUserID)=>{
    return fetch(APIConstants.BASE_URL + APIConstants.GET_CLIENTS, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
      'Content-Type': 'application/json',
      'accessToken' : 'abc',
    },
    body: JSON.stringify({
      'user_id': paramUserID,
    }),
    }).then((response) => response.json())
      .then((responseJson) => {
        //console.log(responseJson) 
        //var count = Object.keys(responseJson.clients).length;
  //let temp_drop_down_client = responseJson.clients;
  //for(var i=0;i<count;i++){
    //console.log(responseJson.clients[i].client) 
    //temp_drop_down_client.push({ value: responseJson.clients[i].client,id: responseJson.clients[i].id});
  //}
        this.setState({
          isLoading: false,
          drop_down_client : responseJson.clients,
        }, function(){

        });

      })
      .catch((error) =>{
        console.error(error);
      });
  }

  prepopulateTaskDetails() {
    //"log_id": 100005,
    let task = this.props.navigation.state.params.task
    this.setState({
      drop_down_client: [{"id": task.client_id,"client": task.client_name}],
      drop_down_projects: [{"id": task.project_id,"project": task.project_name}],
      drop_down_tasks: [{"id": task.task_id,"task": task.task_name}],
      drop_down_subtasks: [{"id": task.sub_task_id,"subtask": task.sub_task_name}],
      selectedClientId: task.client_id,
      selectedProjectId: task.project_id,
      selectedTaskId: task.task_id,
      selectedSubtaskId: task.sub_task_id,
      comment: task.comment,
      startTime : task.start_time.slice(0,5),
      endTime : this.getEndTime(task.start_time.slice(0,5) ,task.duration.slice(0,5) )
    })

  }

  setDefaultProjectSettings() {
    AsyncStorage.getItem('defaultProjectSettings', (err, result) => {
      let json = JSON.parse(result)
      this.setState({
        drop_down_client: json.drop_down_client,
        drop_down_projects: json.drop_down_projects,
        drop_down_tasks: json.drop_down_tasks,
        drop_down_subtasks: json.drop_down_subtasks,
        selectedClientId: json.clientID,
        selectedProjectId: json.projectID,
        selectedTaskId: json.taskID,
        selectedSubtaskId: json.subtaskID
      });
    });
  }

  valueChangedSubTask = (subtaskid)=>{
    this.setState({ selectedSubtaskId: subtaskid ,
      isLoading: true}, function () {
        console.log('Selected_SubTask_ID : '+this.state.selectedSubtaskId)
    })
  }

  valueChangedTask = (taskid)=>{
    this.setState({ selectedTaskId: taskid ,
      isLoading: true}, function () {
        console.log('Selected_Task_ID : '+this.state.selectedTaskId)
    })

    return fetch(APIConstants.BASE_URL + APIConstants.GET_SUB_TASKS, {
      method: 'POST',
      headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'accessToken' : 'abc',
    },
    body: JSON.stringify({
      'userID': this.state.user_id,
      'taskID': taskid,
    }),
    }).then((response) => response.json())
      .then((responseJson) => {
      console.log(responseJson) 
      console.log(APIConstants.BASE_URL + APIConstants.GET_SUB_TASKS)
  //       var count = Object.keys(responseJson.projects).length;
  // let temp_drop_down_projects = [];
  // for(var i=0;i<count;i++){
  //   console.log(responseJson.projects[i].client) 
  //   temp_drop_down_projects.push({ value: responseJson.projects[i].project,id : responseJson.projects[i].id});
  //}
        this.setState({
          isLoading: false,
          drop_down_subtasks : responseJson.subTasks
        }, function(){

        });

      })
      .catch((error) =>{
        console.error(error);
      });
  }

  valueChangedProject = (projectid)=>{
    this.setState({ selectedProjectId: projectid ,drop_down_subtasks : [],
      isLoading: true}, function () {
        console.log('Selected_Project_ID : '+this.state.selectedProjectId)
    })

    //var baseurl2 = 'https://private-245a9-timetrackerapi5.apiary-mock.com/api/timetracker/gettasks'

      //console.log('Selected Client ID : '+this.state.selectedClientId)

    return fetch(APIConstants.BASE_URL + APIConstants.GET_TASKS, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
      'Content-Type': 'application/json',
      'accessToken' : 'abc',
    },
    body: JSON.stringify({
      'userID': this.state.user_id,
      'projectID': projectid,
    }),
    }).then((response) => response.json())
      .then((responseJson) => {
      console.log(responseJson) 
      console.log(APIConstants.BASE_URL + APIConstants.GET_TASKS)
  //       var count = Object.keys(responseJson.projects).length;
  // let temp_drop_down_projects = [];
  // for(var i=0;i<count;i++){
  //   console.log(responseJson.projects[i].client) 
  //   temp_drop_down_projects.push({ value: responseJson.projects[i].project,id : responseJson.projects[i].id});
  //}
        this.setState({
          isLoading: false,
          drop_down_tasks : responseJson.tasks
        }, function(){

        });

      })
      .catch((error) =>{
        console.error(error);
      });
  }

  valueChangedClient = (clientid) => {
    //const id = parseInt(lang)
    this.setState({ selectedClientId: clientid ,drop_down_subtasks :[],drop_down_tasks : [],
      isLoading: true}, function () {
        console.log('Selected_Client_ID : '+this.state.selectedClientId)
    })

      //var baseurl1 = 'https://private-245a9-timetrackerapi5.apiary-mock.com/api/timetracker/getprojects'

      //console.log('Selected Client ID : '+this.state.selectedClientId)

    return fetch(APIConstants.BASE_URL + APIConstants.GET_PROJECTS, {
      method: 'POST',
      headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'accessToken' : 'abc',
    },
    body: JSON.stringify({
      'user_id': this.state.user_id,
      'client_id': clientid,
    }),
    }).then((response) => response.json())
      .then((responseJson) => {
      console.log(responseJson) 
      console.log(APIConstants.BASE_URL + APIConstants.GET_PROJECTS)
  //       var count = Object.keys(responseJson.projects).length;
  // let temp_drop_down_projects = [];
  // for(var i=0;i<count;i++){
  //   console.log(responseJson.projects[i].client) 
  //   temp_drop_down_projects.push({ value: responseJson.projects[i].project,id : responseJson.projects[i].id});
  //}
        this.setState({
          isLoading: false,
          drop_down_projects : responseJson.projects
        }, function(){

        });

      })
      .catch((error) =>{
        console.error(error);
      });
    }

render() {

    return (
      <View style={styles.screen}>
        <View style={styles.container}>
        <View style={{ flexDirection: 'row' }}>
        <View style={{ flex: 1 ,marginRight :5}}>
        <Text style={styles.instructions}>Start Time</Text>
        <DatePicker
        date={this.state.startTime}
        mode="time"
        format="HH:mm"
        showIcon={false}
        confirmBtnText="Confirm"
        cancelBtnText="Cancel"
        customStyles={{
            btnTextConfirm: {
                height: 20
            },
            btnTextCancel: {
                height: 20
            },
            dateInput: {
                marginLeft: 30,
                marginRight: 10
            }
            // ... You can check the source to find the other keys.
        }}
        minuteInterval={10}
        onDateChange={(time) => {this.setState({startTime: time});}}
      />
          </View>
          <View style={{ flex: 1 }}>
          <Text style={styles.instructions}>End Time</Text>
          <DatePicker
          date={this.state.endTime}
          mode="time"
          format="HH:mm"
          showIcon={false}
          confirmBtnText="Confirm"
          cancelBtnText="Cancel"
          customStyles={{
              btnTextConfirm: {
                  height: 20
              },
              btnTextCancel: {
                  height: 20
              },
              dateInput: {
                  marginLeft: 30,
                  marginRight: 10
              }
              // ... You can check the source to find the other keys.
          }}
          minuteInterval={10}
          onDateChange={(time) => {this.setState({endTime: time});}}
        />
          </View>
          </View>

        <Picker style={{height: 50}} mode='dropdown' selectedValue={this.state.selectedClientId} onValueChange={this.valueChangedClient} >
      {this.state.drop_down_client.map((item) => {
        return (
            <Picker.Item label={item.client} value={item.id} key={item.id}/>
        )
    })}
    </Picker>
    <View
  style={{
    borderBottomColor: 'black',
    borderBottomWidth: 1,
  }}/>
  <Picker style={{height: 50}} mode='dropdown' selectedValue={this.state.selectedProjectId} onValueChange={this.valueChangedProject} >
      {this.state.drop_down_projects.map((item) => {
        return (
            <Picker.Item label={item.project} value={item.id} key={item.id}/>
        )
    })}
    </Picker>
    <View
  style={{
    borderBottomColor: 'black',
    borderBottomWidth: 1,
  }}/>
  <Picker style={{height: 50}} mode='dropdown' selectedValue={this.state.selectedTaskId} onValueChange={this.valueChangedTask} >
      {this.state.drop_down_tasks.map((item) => {
        return (
            <Picker.Item label={item.task} value={item.id} key={item.id}/>
        )
    })}
    </Picker>
    <View
  style={{
    borderBottomColor: 'black',
    borderBottomWidth: 1,
  }}/>

  <Picker style={{height: 50}} mode='dropdown' selectedValue={this.state.selectedSubtaskId} onValueChange={this.valueChangedSubTask} >
      {this.state.drop_down_subtasks.map((item) => {
        return (
            <Picker.Item label={item.subtask} value={item.id} key={item.id}/>
        )
    })}
    </Picker>
    <View
  style={{
    borderBottomColor: 'black',
    borderBottomWidth: 1,
  }}/>
  <TextInput
        style={{height: 100, borderColor: 'gray', borderWidth: 1,marginTop : 20,padding:5}}
        onChangeText={(comment) => this.setState({comment})}
        value={this.state.comment}
        placeholder = 'Note'
        //multiline = 'true' 
      />
            <TouchableOpacity
                style={styles.LoginButtonStyle}
                activeOpacity = { .5 }
                onPress={ this.onPressSubmit } >
                <Text style={styles.TextStyle}> Save </Text>
            </TouchableOpacity>


        </View>
      </View>
    );
  }
}

const styles = {
    screen: {
      flex: 1,
      paddingTop: 10,
        paddingLeft: 20,
        paddingRight: 20,
      backgroundColor: '#E8EAF6',
        flexDirection: 'column',
    },

    container: {
      marginHorizontal: 4,
      marginVertical: 8,
      paddingHorizontal: 8,
    },

    text: {
      textAlign: 'center',
    },

    instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5
  },
    LoginButtonStyle: {

        marginTop:40,
        paddingTop:10,
        paddingBottom:24,
        marginLeft:95,
        marginRight:30,
        backgroundColor:'#00BCD4',
        borderRadius:20,
        borderWidth: 1,
        borderColor: '#00BCD4',
        width: 160,
        height: 40,
    },
    TextStyle:{
        color:'#fff',
        textAlign:'center',
        height: 30,
    },
  };

  

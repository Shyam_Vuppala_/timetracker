import React, { Component } from 'react';
import CalendarStrip from 'react-native-calendar-strip';
import moment from 'moment';
import PropTypes from 'prop-types';
import Dashboard from './Dashboard'

import {
  StyleSheet,
  Image,
  View,
  TouchableHighlight,
  FlatList,
  Text,
  Platform,
  TouchableOpacity,
  Animated,
  ScrollView,
  Button,
  Dimensions,
  ActivityIndicator,

} from 'react-native';

export default class Task extends Component {

  static propTypes = {
    onRemoveListItem: PropTypes.func,
  }

  render() {
    const {onRemoveListItem} = this.props
    let task = this.props.task
    let duration = task.duration.slice(0, 5)
    let hours = Number(duration.slice(0,2))
    let minutes = Number(duration.slice(3,5))
    var durationString = ''
    if (minutes == 0) {
      if (hours == 1) {
        durationString = hours + ' hr'
      } else {
        durationString = hours + ' hrs'
      }
    } else {
      if (hours == 0) {
        durationString = minutes + ' mins'
      } else if (hours == 1) {
        durationString = hours + ':' + minutes + ' hrs'
      } else {
        durationString = hours + ':' + minutes + ' hrs'
      }
    }
    
    let comment = (task.comment == null) ? '' : task.comment
    
    return (
      <TouchableHighlight
        onPress={this.props.onPress}
        underlayColor='#dddddd'>
        <View>
          <View style={styles.rowContainer}>
            <View style={styles.durationHeader}>
              <Text style={styles.duration}
                numberOfLines={0}>{durationString}</Text>
            </View>
            <View style={styles.textContainer}>
              <Text style={styles.task}>{task.task_name}</Text>
              <Text style={styles.subTask}
                numberOfLines={1}>{task.sub_task_name}</Text>
              <Text style={styles.comment}>{comment}</Text>
            </View>
            <View>
              <TouchableOpacity onPress={onRemoveListItem} >
                <Image style={{height:18, width:18}} source={require('./resources/images/crossIcon.png')}/>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </TouchableHighlight>
    );
  }

}

const deviceWidth = Dimensions.get('window').width
const styles = StyleSheet.create({

  durationHeader: {
    width: 82,
    height: 82,
    marginRight: 5,
    marginLeft: 10,
    justifyContent:'center',
  },
  textContainer: {
    flex: 1,
    justifyContent:'center',
    marginTop: 5,
    marginLeft: 10
  },
  duration: {
    fontSize: 18,
    color: 'black',
    alignItems:'center',
    justifyContent:'center',
    textAlign:'left'
  },
  task: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#2F95D6'
  },
  subTask: {
    fontSize: 15,
    color: 'black',
    marginTop: 5
  },
  comment: {
    fontSize: 19,
    color: '#656565'
  },
  rowContainer: {
    flexDirection: 'row',
    padding:10,
    marginLeft: 10,
    marginRight: 10,
    marginTop: 5,
    marginBottom: 5,
    borderWidth: 1.0,
    borderRadius: 10,
    borderColor: '#d6d7da',
  },
});

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
'use strict';
import React, { Component } from 'react';
import {
  StackNavigator,
} from 'react-navigation';
import {
  AsyncStorage,
} from 'react-native';
import LoginPage from './LoginPage';
import Settings from './Settings';
import AddTask from './AddTask';
import Dashboard from './Dashboard'

const Login = StackNavigator({
  Home: { screen: LoginPage },
  Results: { screen: Settings },
  AddTask: { screen: AddTask },
  Dashboard: {screen: Dashboard},
},{
  initialRouteName : 'Home',
  headerTintColor: '#ffffff',
    headerStyle: {
      backgroundColor: '#2F95D6',
      borderBottomColor: '#ffffff',
      borderBottomWidth: 3,
    },
    headerTitleStyle: {
      fontSize: 18,
    },
});

const Home = StackNavigator({
  Home: { screen: LoginPage },
  Results: { screen: Settings },
  AddTask: { screen: AddTask },
  Dashboard: {screen: Dashboard},
},{
  initialRouteName : 'Dashboard',
});

class App extends Component {
    constructor(props) {
      super(props);
      this.state = {
        isUserLoggedIn: false
      }
    }

    componentDidMount(){
      AsyncStorage.getItem('user_id', (err, result) => {
      let json = JSON.parse(result)
      if (json != null) {
        this.setState({
          isUserLoggedIn : true
        });
      }
        
      });
    }

    render(){
      if (this.state.isUserLoggedIn == true ){
         return (
            <Home/>
         ) 
       } else {
          return(
             <Login/>
           ) 
       }
  }
}

export default App;

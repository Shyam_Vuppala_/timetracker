import { AppRegistry } from 'react-native';
import { YellowBox } from 'react-native';
import App from './App';

AppRegistry.registerComponent('TimeTracker', () => App);

YellowBox.ignoreWarnings([
    'Warning: componentWillUpdate is deprecated',
    'Warning: componentWillMount is deprecated',
    'Warning: componentWillReceiveProps is deprecated',
    'Module RCTImageLoader requires',
]);
